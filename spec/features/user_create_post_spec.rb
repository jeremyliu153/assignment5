require 'rails_helper'

feature "Create post" do 
	scenario "Successfully" do
		visit root_path

		click_on 'New Post'
		fill_in 'Title', with: 'Ruby'
		fill_in 'Content', with: 'rspec'
		click_on 'Create Post'

		expect(page).to have_content('Post was successfully created.')
		
	end

	scenario "Fail" do
		visit root_path
		click_on 'New Post'
		fill_in 'Title', with: ''
		fill_in 'Content', with: ''
		click_on 'Create Post'

		expect(page).to have_current_path('/posts')

		expect(page).to have_content("Title can't be blank")
		
	end
end