require 'rails_helper'

feature "Delete post", driver: :selenium_chrome_headless do 
	scenario "Should not delete if not accepted" do
		visit root_path
		post = Post.create!(title: "Post Title", content: "Post Content")
		visit posts_path
		click_on "Destroy"
		page.dismiss_prompt
		expect(page).to have_content("Post Title")
		
	end
	scenario "Should delete if accepted" do
		visit root_path
		post = Post.create!(title: "Post Title", content: "Post Content")
		visit posts_path
		click_on "Destroy"
		page.accept_prompt
		expect(page).not_to have_content("Post Title")
		
	end
end